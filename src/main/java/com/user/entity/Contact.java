package com.user.entity;

public class Contact {

    private Long contactId;
    private String emailId;
    private String contactName;
    private User userid;

    public Contact(Long contactId, String emailId, String contactName, User userid) {
        this.contactId = contactId;
        this.emailId = emailId;
        this.contactName = contactName;
        this.userid = userid;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }
}
