package com.user.entity;

import java.util.List;

public class User {
	
	private Long userId;
	private String userName;
	private String phoneNo;

	List<Contact> contactList;

	public User(Long userId, String userName, String phoneNo, List<Contact> contactList) {
		this.userId = userId;
		this.userName = userName;
		this.phoneNo = phoneNo;
		this.contactList = contactList;
	}

	public User(Long userId, String userName, String phoneNo) {
		this.userId = userId;
		this.userName = userName;
		this.phoneNo = phoneNo;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public List<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(List<Contact> contactList) {
		this.contactList = contactList;
	}
}
