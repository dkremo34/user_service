package com.user.service;

import com.user.entity.User;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    List<User> userList = List.of(
            new User(1L,"dheeraj","987654345"),
            new User(2L,"amit","999999999999"),
            new User(3L,"vivek","63998888888"),
            new User(4L,"sunil","87633333333")
    );

    @Override
    public User getUser(Long id) {
        return userList.stream().filter(user -> user.getUserId().equals(id)).findAny().orElse(null);
    }
}